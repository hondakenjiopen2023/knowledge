<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ログイン</title>
		<%@ include file="header.jsp"%>
		<style>
			.grid {
				display: grid;
				grid-template-columns: 43% 57%;
				margin-bottom: 5px;
			 }
			
			.grid input {
				width: 40%;
			 }
		</style>
	</head>
	<body>
		<%@ include file="navbar.jsp"%>
	
		<div class="container">
			<h4 class="title">ログイン</h4>
			<div class="error-message">
				<c:out value="${ errorMessage }"/>
			</div>
			<form method="post" action="login" class="center">
				<div class="grid">
					<label class="head">メールアドレス</label> <input type="text" name="id" />
				</div>
				<div class="grid">
					<label class="head">パスワード</label> <input type="password"
						name="password" />
				</div>
				<div>
					<input type="submit" class="btn btn-primary" value="ログイン"/>
				</div>
			</form>
		</div>
	</body>
</html>