<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ナレッジ編集</title>
<%@ include file="header.jsp"%>
</head>
<body>
	<%@ include file="navbar.jsp"%>
	<div class="container">
		<h4 class="title">ナレッジ編集</h4>
		<br>
		<c:forEach items="${ errorMessageList }" var="errorMessage">
			<div class="error-message">
				<c:out value="${ errorMessage }" />
			</div>
		</c:forEach>

		<form action="edit-knowledge" method="post">
			<div class="grid">
				<label class="head">チャンネル</label>
				<div>
					<select name="channelId">
						<c:forEach items="${channelList}" var="channel">
							<option value="${channel.channelId}">${channel.channelName}</option>
						</c:forEach>
					</select>
				</div>
			</div>
			<div class="grid">
				<label class="head">ナレッジ</label> <input type="hidden"
					name="knowledgeId" value="${ knowledgeId }" /> <input
					type="hidden" name="updateNumber"
					value="${ knowledgeDto.updateNumber }" />
				<div>
					<textarea name="knowledge" style="resize: none;" maxlength="65535"
						rows="11"><c:out value="${ knowledgeDto.knowledge }" /></textarea>
				</div>
			</div>
			<div class="center">
				<button type="reset"
					class="btn btn-secondary btn-secondary:hover btn:active">リセット</button>
				<button type="submit"
					class="btn btn-primary btn-primary:hover btn:active">編集</button>
				<button type="submit" formaction="delete-knowledge"
					class="btn btn-danger  btn-danger:hover btn:active" id="deleteBtn"
					onclick="return onDelete()">削除</button>
			</div>
		</form>
	</div>
	<script>
		function onDelete() {
			// Display a confirmation dialog
			return confirm("本当に削除しますか?");
		}
	</script>
</body>
</html>