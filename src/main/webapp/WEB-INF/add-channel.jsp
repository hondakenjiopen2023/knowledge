<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>チャンネル登録</title>
	<%@ include file="header.jsp"%>
</head>
<body>
	<%@ include file="navbar.jsp"%>

	<div class="container">
		<h4 class="title">チャンネル登録</h4>
		<br>
		<c:forEach items="${ errorMessageList }" var="errorMessage">
			<div class="error-message">
				<c:out value="${ errorMessage }" />
			</div>
		</c:forEach>
		<br>
		<form action="add-channel" method="post">
			<div class="grid">
				<label class="head">チャンネル</label>
				<div>
					<input type="text" name="channelName" maxlength="30"
						value="${ channelDto.channelName }" />
				</div>
			</div>
			<div class="grid">
				<label class="head">概要</label>
				<div>
					<textarea name="overview" style="resize: none;" maxlength="200"
						rows="4"><c:out value="${ channelDto.overview }" /></textarea>
				</div>
			</div>
			<div class="center">
				<button type="submit" class="btn btn-primary btn:active">登録</button>
			</div>
		</form>
	</div>
</body>
</html>