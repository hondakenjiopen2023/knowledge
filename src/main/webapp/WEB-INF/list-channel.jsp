<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>チャンネル一覧</title>
<%@ include file="header.jsp"%>

</head>
<body>
	<%@ include file="navbar.jsp"%>
	<div class="container">
		<h4 class="title">チャンネル一覧</h4>
		<div class="message">
			<c:out value="${ message }" />
		</div>
		<table class="table">
			<tr>
				<th>#</th>
				<th>チャンネル</th>
				<th>概要</th>
				<c:if test="${ not empty user }">
					<th><c:choose>
							<c:when test="${ not empty user }">
								<a href="add-channel" class="btn btn-primary btn-sm">登録</a>
							</c:when>
							<c:otherwise>
								<br>
							</c:otherwise>
						</c:choose></th>
					<th scope="col"><br></th>
				</c:if>
			</tr>
			<c:forEach items="${ channelList }" var="channelDto"
				varStatus="status">
				<tr>
					<td><c:out value="${ status.count }" /></td>
					<td><a
						href="list-knowledge?channelId=${ channelDto.channelId }">${ channelDto.channelName }</a></td>
					<td><c:out value="${ channelDto.overview }" /></td>
					<c:if
						test="${ user.userId == channelDto.userId || user.administratorFlg }">
						<td>
							<form action="edit-channel" method="get">
								<input type="hidden" name="channelId"
									value="${ channelDto.channelId }">
								<button type="submit" class="btn btn-warning btn-sm btn:active">編集</button>
							</form>
						</td>
						<td>
							<form action="delete-channel" method="post">
								<input type="hidden" name="channelId"
									value="${ channelDto.channelId }"> <input type="hidden"
									name="updateNumber" value="${ channelDto.updateNumber }">
								<button type="submit" class="btn btn-danger btn-sm btn:active"
									value="${ channelDto.channelName }"
									onclick="return btnDelete('${ channelDto.channelName }')">削除</button>
							</form>
						</td>
					</c:if>
					<c:if
						test="${ user.userId != channelDto.userId && user.administratorFlg == false }">
						<td><br></td>
						<td><br></td>
					</c:if>
				</tr>
			</c:forEach>
		</table>
	</div>
</body>
<script type="text/javascript">
<!--
	// 削除ボタン押下
	function btnDelete(channelName) {
		return confirm("「" + channelName + "」チャンネルを本当に削除しますか？");
	}
// -->
</script>
</html>