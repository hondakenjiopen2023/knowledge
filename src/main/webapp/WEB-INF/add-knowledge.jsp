<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ナレッジー一覧</title>
<%@ include file="header.jsp"%>
</head>
<body>
	<%@ include file="navbar.jsp"%>
	<div class="container">
		<h4 class="title">ナレッジ追加</h4>
		<br>
		<c:forEach items="${ errorMessageList }" var="errorMessage">
			<div class="error-message">
				<c:out value="${ errorMessage }" />
			</div>
		</c:forEach>
		<form action="add-knowledge" method="post">
			<div class="grid">
				<label class="head">チャンネル</label>
				<div>
					<select name="channelId">
						<c:forEach items="${channelList}" var="channel">
							<option value="${channel.channelId}">${channel.channelName}</option>
						</c:forEach>
					</select>
				</div>
			</div>
			<div class="grid">
				<label class="head">ナレッジ</label>
				<div>
					<textarea name="knowledge" style="resize: none;" maxlength="65535"
						rows="11"><c:out value="" /></textarea>
				</div>
			</div>
			<div class="center">
				<button type="submit" class="btn btn-primary btn:active">追加</button>
			</div>
		</form>
	</div>
</body>
</html>