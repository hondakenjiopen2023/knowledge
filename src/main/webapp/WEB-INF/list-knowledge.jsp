<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ナレッジ一覧</title>
<%@ include file="header.jsp"%>
<style>
</style>
</head>
<body>
	<%@ include file="navbar.jsp"%>
	<div class="container">
		<h4 class="title">ナレッジ一覧</h4>
		<div class="message">
			<c:out value="${ addMessage }" />
		</div>
		<div class="message">
			<c:out value="${ updateMessage }" />
		</div>
		<div class="message">
			<c:out value="${ deleteMessage }" />
		</div>

		<div class="center">
			<form action="search-knowledge" method="get">
				<input class="input-search gray-input" type="text" name="keyword"
					placeholder="ナレッジ検索" value="${ keyword }" />
				<button type="submit" class="btn btn-primary btn-primary:hover btn:active">検索</button>
				<a href="add-knowledge">ナレッジ追加</a>
			</form>
		</div>

		<c:choose>
			<c:when test="${ knowledgeList.size() == 0 }">
				<h3 class="center">検索キーワードに該当するナレッジありません。</h3>
			</c:when>
			<c:otherwise>
				<table class="table">
					<th>チャンネル名</th>
					<th>ナレッジ</th>
					<th>最終更新時間</th>
					</tr>
					<c:forEach items="${knowledgeList}" var="knowledgeItem">
						<tr>
							<td><a
								href="list-knowledge?channelId=${ knowledgeItem.channelId }">${ knowledgeItem.channelName }</a>
							</td>
							<td><c:out value="${ knowledgeItem.knowledge }" /></td>

							<c:choose>
								<c:when
									test="${ user.userId == knowledgeItem.userId || user.administratorFlg == true }">
									<td><a
										href="edit-knowledge?knowledgeId=${ knowledgeItem.knowledgeId }">${ knowledgeItem.updateAt }</a>
									</td>
								</c:when>
								<c:otherwise>
									<td><c:out value="${ knowledgeItem.updateAt }" /></td>
								</c:otherwise>
							</c:choose>
						</tr>
					</c:forEach>
				</table>
			</c:otherwise>
		</c:choose>
	</div>
</body>
</html>