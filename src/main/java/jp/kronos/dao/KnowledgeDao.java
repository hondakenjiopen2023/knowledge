package jp.kronos.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import jp.kronos.dto.KnowledgeDto;

/**
 * ナレッジテーブルのDataAccessObject
 * 
 * @author Mr.X
 */
public class KnowledgeDao {

	/** コネクション */
	protected Connection conn;

	public KnowledgeDao(Connection conn) {
		this.conn = conn;
	}

	public void deleteByChannelId(int channelId) throws SQLException {
		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" delete from KNOWLEDGE");
		sb.append("       where CHANNEL_ID = ?");

		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			// プレースホルダーに値をセットする
			ps.setInt(1, channelId);

			// SQLを実行する
			ps.executeUpdate();
		}
	}

	public List<KnowledgeDto> selectByChannelId(int channelId) throws SQLException {
		String sql = "select channel.CHANNEL_NAME, knowledge.KNOWLEDGE_ID, knowledge.KNOWLEDGE, knowledge.USER_ID, knowledge.UPDATE_AT from knowledge inner join channel on channel.CHANNEL_ID = knowledge.CHANNEL_ID where channel.CHANNEL_ID = ? order by knowledge.KNOWLEDGE_ID desc";
		List<KnowledgeDto> knowledgeList = new ArrayList<>();
		try (PreparedStatement ps = conn.prepareStatement(sql)) {
			// プレースホルダーに値をセットする
			ps.setInt(1, channelId);

			// SQL文を実行する
			ResultSet rs = ps.executeQuery();
			
			while (rs.next()) {
				KnowledgeDto knowledgeDto = new KnowledgeDto();
				knowledgeDto.setChannelName(rs.getString("CHANNEL_NAME"));
				knowledgeDto.setKnowledge(rs.getString("KNOWLEDGE"));
				knowledgeDto.setChannelId(channelId);
				knowledgeDto.setUserId(rs.getInt("USER_ID"));
				knowledgeDto.setKnowledgeId(rs.getInt("KNOWLEDGE_ID"));
				knowledgeDto.setUpdateAt(rs.getTimestamp("UPDATE_AT"));
				knowledgeList.add(knowledgeDto);
			}
			return knowledgeList;
		}

	}

	public List<KnowledgeDto> selectAll() throws SQLException {
		String sql = "select channel.CHANNEL_NAME, knowledge.KNOWLEDGE, knowledge.UPDATE_AT, channel.CHANNEL_ID from knowledge inner join channel on channel.CHANNEL_ID = knowledge.CHANNEL_ID order by knowledge.KNOWLEDGE_ID desc";
		List<KnowledgeDto> knowledgeList = new ArrayList<>();
		try (PreparedStatement ps = conn.prepareStatement(sql)) {

			// SQL文を実行する
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				KnowledgeDto knowledgeDto = new KnowledgeDto();
				knowledgeDto.setChannelName(rs.getString("CHANNEL_NAME"));
				knowledgeDto.setKnowledge(rs.getString("KNOWLEDGE"));
				knowledgeDto.setUpdateAt(rs.getTimestamp("UPDATE_AT"));
				knowledgeDto.setChannelId(rs.getInt("CHANNEL_ID"));
				knowledgeList.add(knowledgeDto);
			}
			return knowledgeList;
		}
	}

	public List<KnowledgeDto> selectByQueries(String[] keywords) throws SQLException {
		StringBuilder sql = new StringBuilder();
		sql.append(
				"select channel.CHANNEL_NAME, knowledge.KNOWLEDGE, knowledge.UPDATE_AT from knowledge inner join channel on channel.CHANNEL_ID = knowledge.CHANNEL_ID ");
		for (int i = 0; i < keywords.length; i++)
			sql.append("AND knowledge.KNOWLEDGE LIKE ? ");
		sql.append("order by knowledge.KNOWLEDGE_ID desc");
		List<KnowledgeDto> knowledgeList = new ArrayList<>();
		try (PreparedStatement ps = conn.prepareStatement(sql.toString())) {
			// プレースホルダーに値をセットする
			for (int i = 0; i < keywords.length; i++)
				ps.setString(i + 1, "%" + keywords[i] + "%");
			// SQL文を実行する
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				KnowledgeDto knowledgeDto = new KnowledgeDto();
				knowledgeDto.setChannelName(rs.getString("CHANNEL_NAME"));
				knowledgeDto.setKnowledge(rs.getString("KNOWLEDGE"));
				knowledgeDto.setUpdateAt(rs.getTimestamp("UPDATE_AT"));
				knowledgeList.add(knowledgeDto);
			}
		}
		return knowledgeList;
	}

	public List<KnowledgeDto> selectChannelList() throws SQLException {
		String sql = "select CHANNEL_NAME, CHANNEL_ID from channel";
		List<KnowledgeDto> channelList = new ArrayList<>();
		try (PreparedStatement ps = conn.prepareStatement(sql)) {
			// SQL文を実行する
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				KnowledgeDto knowledgeDto = new KnowledgeDto();
				knowledgeDto.setChannelName(rs.getString("CHANNEL_NAME"));
				knowledgeDto.setChannelId(rs.getInt("CHANNEL_ID"));
				channelList.add(knowledgeDto);
			}
			return channelList;
		}
	}

	public int insert(KnowledgeDto knowledgeDto) throws SQLException {
		// SQL文を作成する
		String sql = "insert into KNOWLEDGE(KNOWLEDGE, CHANNEL_ID, USER_ID, UPDATE_NUMBER) values(?, ?, ?, 0)";

		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sql)) {
			// プレースホルダーに値をセットする
			ps.setString(1, knowledgeDto.getKnowledge());
			ps.setInt(2, knowledgeDto.getChannelId());
			ps.setInt(3, knowledgeDto.getUserId());

			// SQLを実行する
			return ps.executeUpdate();
		}
	}

	public KnowledgeDto selectByKnowledgeId(int knowledgeId) throws SQLException {
		String sql = "SELECT KNOWLEDGE, USER_ID, CHANNEL_ID, UPDATE_NUMBER FROM KNOWLEDGE WHERE knowledge_id = ?";
		KnowledgeDto knowledgeDto = null;
		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sql)) {
			// プレースホルダーに値をセットする
			ps.setInt(1, knowledgeId);

			// SQL文を実行する
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				knowledgeDto = new KnowledgeDto();
				knowledgeDto.setChannelId(rs.getInt("CHANNEL_ID"));
				knowledgeDto.setUserId(rs.getInt("USER_ID"));
				knowledgeDto.setKnowledge(rs.getString("knowledge"));
				knowledgeDto.setUpdateNumber(rs.getInt("UPDATE_NUMBER"));
			}
		}
		return knowledgeDto;
	}

	public int update(KnowledgeDto knowledgeDto) throws SQLException {
		// SQL文を作成する
		String sql = "UPDATE KNOWLEDGE set CHANNEL_ID = ?, KNOWLEDGE = ?,UPDATE_NUMBER = UPDATE_NUMBER + 1, UPDATE_AT = CURRENT_TIMESTAMP() where KNOWLEDGE_ID = ? and UPDATE_NUMBER = ?";
		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sql)) {

			// プレースホルダーに値をセットする
			ps.setInt(1, knowledgeDto.getChannelId());
			ps.setString(2, knowledgeDto.getKnowledge());
			ps.setInt(3, knowledgeDto.getKnowledgeId());
			ps.setInt(4, knowledgeDto.getUpdateNumber());
			// SQLを実行する
			return ps.executeUpdate();
		}
	}

	public int deleteByKnowledgeId(KnowledgeDto knowledgeDto) throws SQLException {
		// SQL文を作成する
		String sql = "DELETE FROM KNOWLEDGE WHERE KNOWLEDGE_ID = ? and UPDATE_NUMBER = ?";
		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sql)) {
			// プレースホルダーに値をセットする
			ps.setInt(1, knowledgeDto.getKnowledgeId());
			ps.setInt(2, knowledgeDto.getUpdateNumber());

			// SQLを実行する
			return ps.executeUpdate();
		}
	}

}
