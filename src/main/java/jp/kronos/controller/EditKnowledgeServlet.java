package jp.kronos.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.kronos.DataSourceManager;
import jp.kronos.FieldValidator;
import jp.kronos.dao.ChannelDao;
import jp.kronos.dao.KnowledgeDao;
import jp.kronos.dto.ChannelDto;
import jp.kronos.dto.KnowledgeDto;
import jp.kronos.dto.UserDto;

@WebServlet("/edit-knowledge")
public class EditKnowledgeServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// セッションを取得する
		HttpSession session = request.getSession(false);

		// ログインユーザ情報を取得する
		UserDto user = (UserDto) session.getAttribute("user");

		//  セッションにログインしているユーザ情報が入っていない場合
		if (session == null || user == null) {
			// トップページ(チャンネル一覧)に遷移する
			request.getRequestDispatcher("index.jsp").forward(request, response);
			return;
		}

		// エラーメッセージをリクエストに保持する
		request.setAttribute("errorMessageList", session.getAttribute("errorMessageList"));
		session.removeAttribute("errorMessageList");

		// セッションスコープ内のナレッジ情報を取得する
		KnowledgeDto knowledgeDto = (KnowledgeDto) session.getAttribute("KnowledgeDto");

		// セッションスコープにナレッジ情報がある場合
		if (knowledgeDto != null) {
			// セッションスコープからナレッジ情報を削除する
			session.removeAttribute("knowledgeDto");
		} else {
			// フォームのデータを取得する
			request.setCharacterEncoding("UTF-8");
			int knowledgeId = Integer.parseInt(request.getParameter("knowledgeId"));

			// コネクションを取得する
			try (Connection conn = DataSourceManager.getConnection()) {
				// ナレッジ情報を取得する
				KnowledgeDao knowledgeDao = new KnowledgeDao(conn);
				//  ナレッジ情報を取得する
				knowledgeDto = knowledgeDao.selectByKnowledgeId(knowledgeId);

				// ログインユーザに管理権限がない、かつナレッジのユーザIDと一致しない場合
				if (!user.isAdministratorFlg() && knowledgeDto.getUserId() != user.getUserId()) {
					response.sendRedirect("access-denied.jsp");
					return;
				}
				//  取得したナレッジ情報をリクエストスコープに保持する
				request.setAttribute("knowledgeDto", knowledgeDto);
				request.setAttribute("knowledgeId", knowledgeId);

				//  チャンネル情報リストを取得し、リクエストスコープに保持する
				ChannelDao channelDao = new ChannelDao(conn);
				List<ChannelDto> channelList = channelDao.selectAll();

				// チャンネルIDに該当するナレッジ情報リストを取得する
				request.setAttribute("channelList", channelList);

			} catch (SQLException | NamingException e) {
				// 例外メッセージを出力表示
				e.printStackTrace();
				// システムエラー画面に遷移する
				response.sendRedirect("system-error.jsp");
				return;
			}
		}

		// ナレッジ編集画面に遷移する
		request.getRequestDispatcher("WEB-INF/edit-knowledge.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// セッションを取得する
		HttpSession session = request.getSession(false);

		// ログインユーザ情報を取得する
		UserDto user = (UserDto) session.getAttribute("user");

		//  セッションにログインしているユーザ情報が入っていない場合
		if (session == null || user == null) {
			// トップページ(チャンネル一覧)に遷移する
			request.getRequestDispatcher("index.jsp").forward(request, response);
			return;
		}
		// フォームのデータ（チャンネルID、ナレッジID、ナレッジ、更新番号）を取得する
		request.setCharacterEncoding("UTF-8");
		KnowledgeDto knowledgeDto = new KnowledgeDto();
		knowledgeDto.setKnowledgeId(Integer.parseInt(request.getParameter("knowledgeId")));
		knowledgeDto.setChannelId(Integer.parseInt(request.getParameter("channelId")));
		knowledgeDto.setKnowledge(request.getParameter("knowledge"));
		knowledgeDto.setUpdateNumber(Integer.parseInt(request.getParameter("updateNumber")));

		// 入力チェック
		List<String> errorMessageList = FieldValidator.knowledgeValidation(knowledgeDto);
		if (errorMessageList.size() != 0) {
			// チャンネル編集画面に遷移する
			session.setAttribute("errorMessageList", errorMessageList);
			session.setAttribute("knowledgeDto", knowledgeDto);
			response.sendRedirect("edit-knowledge?knowledgeId=" + request.getParameter("knowledgeId"));
			return;
		}

		// コネクションを取得する
		try (Connection conn = DataSourceManager.getConnection()) {
			KnowledgeDao knowledgeDao = new KnowledgeDao(conn);

			// チャンネルを更新する
			knowledgeDao.update(knowledgeDto);

			//  メッセージ（更新しました）をセッションスコープに保持する
			session.setAttribute("updateMessage", "更新しました");

			// チャンネル一覧画面に遷移する
			response.sendRedirect("list-knowledge?channelId=" + request.getParameter("channelId"));

		} catch (SQLException | NamingException e) {
			// 例外メッセージを出力表示
			e.printStackTrace();

			// システムエラー画面に遷移する
			response.sendRedirect("system-error.jsp");
		}
	}
}
