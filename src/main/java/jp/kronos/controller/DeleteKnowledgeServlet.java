package jp.kronos.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.kronos.BusinessLogicException;
import jp.kronos.DataSourceManager;
import jp.kronos.dao.KnowledgeDao;
import jp.kronos.dto.KnowledgeDto;
import jp.kronos.dto.UserDto;

@WebServlet("/delete-knowledge")
public class DeleteKnowledgeServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// トップページ(チャンネル一覧)に遷移する
		request.getRequestDispatcher("index.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// セッションを取得する
		HttpSession session = request.getSession(false);
		// ログインユーザ情報を取得する
		UserDto user = (UserDto) session.getAttribute("user");
		if (session == null || user == null) {
			// トップページ(チャンネル一覧)に遷移する
			request.getRequestDispatcher("index.jsp").forward(request, response);
			return;
		}

		// フォームのデータを取得する
		request.setCharacterEncoding("UTF-8");
		KnowledgeDto knowledgeDto = new KnowledgeDto();
		knowledgeDto.setChannelId(Integer.parseInt(request.getParameter("channelId")));
		knowledgeDto.setKnowledge(request.getParameter("knowledge"));
		knowledgeDto.setUpdateNumber(Integer.parseInt(request.getParameter("updateNumber")));
		knowledgeDto.setKnowledgeId(Integer.parseInt(request.getParameter("knowledgeId")));

		try (Connection conn = DataSourceManager.getConnection()) {
			try {
				// トランザクションを開始する
				conn.setAutoCommit(false);

				// ナレッジ情報を削除する
				KnowledgeDao knowledgeDao = new KnowledgeDao(conn);

				int count = knowledgeDao.deleteByKnowledgeId(knowledgeDto);

				if (count == 0) {
					throw new BusinessLogicException("排他制御（楽観ロック）例外");
				}

				// コミットする
				conn.commit();

				session.setAttribute("deleteMessage", "削除しました");
			} catch (BusinessLogicException e) {
				// ロールバックする
				conn.rollback();
				session.setAttribute("deleteMessage", "コンテンツを削除できませんでした。 - 排他制御");
			} catch (SQLException e) {
				// ロールバックする
				conn.rollback();
				throw e;
			} finally {
				conn.setAutoCommit(true);
			}

			// チャンネル一覧画面に遷移する
			response.sendRedirect("list-knowledge?channelId=" + knowledgeDto.getChannelId());
		} catch (SQLException | NamingException e) {

			// 例外メッセージを出力表示
			e.printStackTrace();

			// システムエラー画面に遷移する
			response.sendRedirect("system-error.jsp");
		}
	}
}
