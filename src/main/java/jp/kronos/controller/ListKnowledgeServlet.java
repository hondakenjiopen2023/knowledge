package jp.kronos.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.kronos.DataSourceManager;
import jp.kronos.dao.KnowledgeDao;
import jp.kronos.dto.KnowledgeDto;
import jp.kronos.dto.UserDto;

@WebServlet("/list-knowledge")
public class ListKnowledgeServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//Sessionを取得する
		HttpSession session = request.getSession(true);

		// セッションからログインユーザ情報を取得する
		UserDto user = (UserDto) session.getAttribute("user");

		// チャンネルIDを取得する
		String channelIdStr = request.getParameter("channelId");
		int channelId = Integer.parseInt(channelIdStr);

		try (Connection conn = DataSourceManager.getConnection()) {
			// チャンネルIDに該当するナレッジ情報リストを取得する
			KnowledgeDao knowledgeDao = new KnowledgeDao(conn);
			List<KnowledgeDto> knowledgeList = knowledgeDao.selectByChannelId(channelId);

			// チャンネルIDに該当するナレッジ情報リストを取得する
			request.setAttribute("knowledgeList", knowledgeList);
			request.setAttribute("channelId", channelId);

			// メッセージをリクエストに保持する
			request.setAttribute("addMessage", session.getAttribute("addMessage"));
			request.setAttribute("updateMessage", session.getAttribute("updateMessage"));
			request.setAttribute("deleteMessage", session.getAttribute("deleteMessage"));
			session.removeAttribute("deleteMessage");
			session.removeAttribute("addMessage");
			session.removeAttribute("updateMessage");

			// list-knowledge.jspに転送する
			request.getRequestDispatcher("/WEB-INF/list-knowledge.jsp").forward(request, response);

		} catch (SQLException | NamingException e) {
			System.err.println(e.getMessage());

			// システムエラー画面に遷移する
			response.sendRedirect("system-error.jsp");
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}
}
