package jp.kronos.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.kronos.DataSourceManager;
import jp.kronos.dao.KnowledgeDao;
import jp.kronos.dto.KnowledgeDto;

@WebServlet("/search-knowledge")

public class SearchKnowledgeServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// 検索キーワードを取得する
		String keyword = request.getParameter("keyword");

		// セッションを取得する
		HttpSession session = request.getSession(true);

		try (Connection conn = DataSourceManager.getConnection()) {
			// 検索キーワードが空文字の場合、 ナレッジ情報リストを全件検索する
			KnowledgeDao knowledgeDao = new KnowledgeDao(conn);
			List<KnowledgeDto> knowledgeList = new ArrayList<>();
			if (keyword.isEmpty()) {
				knowledgeList = knowledgeDao.selectAll();
			} else {
				// 検索キーワードをリクエストスコープに保持する
				String[] keywordArray = keyword.split(" ");
				request.setAttribute("keyword", keyword);
				knowledgeList = knowledgeDao.selectByQueries(keywordArray);
			}
			//  リクエストスコープにナレッジ情報リストを保持する
			request.setAttribute("knowledgeList", knowledgeList);
			//  list-knowledge.jspに転送する
			request.getRequestDispatcher("WEB-INF/list-knowledge.jsp").forward(request, response);
		} catch (SQLException | NamingException e) {
			System.err.println(e.getMessage());
		}
	}

}
