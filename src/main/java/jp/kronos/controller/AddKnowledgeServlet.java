package jp.kronos.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.kronos.DataSourceManager;
import jp.kronos.FieldValidator;
import jp.kronos.dao.ChannelDao;
import jp.kronos.dao.KnowledgeDao;
import jp.kronos.dto.ChannelDto;
import jp.kronos.dto.KnowledgeDto;
import jp.kronos.dto.UserDto;

@WebServlet("/add-knowledge")
public class AddKnowledgeServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//  セッションにログインしているユーザ情報が入っていない場合、index.jspに遷移する
		HttpSession session = request.getSession();
		if (session == null || session.getAttribute("user") == null) {
			// トップページ(チャンネル一覧)に遷移する
			request.getRequestDispatcher("index.jsp").forward(request, response);
			return;
		}

		// チャンネル情報リストを取得し、リクエストスコープに保持する
		try (Connection conn = DataSourceManager.getConnection()) {
			// チャンネル名を取得する
			ChannelDao channelDao = new ChannelDao(conn);
			List<ChannelDto> channelList = channelDao.selectAll();

			// チャンネルIDに該当するナレッジ情報リストを取得する
			request.setAttribute("channelList", channelList);

			//  セッションスコープ内のメッセージをリクエストスコープに保持
			request.setAttribute("errorMessageList", session.getAttribute("errorMessageList"));
			// セッションスコープ内からメッセージを削除する
			session.removeAttribute("errorMessageList");

			// add-knowledge.jspに転送する
			request.getRequestDispatcher("/WEB-INF/add-knowledge.jsp").forward(request, response);
		} catch (SQLException | NamingException e) {

			// 例外メッセージを出力表示
			e.printStackTrace();
			
			// システムエラー画面に遷移する
			response.sendRedirect("system-error.jsp");
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//  セッションにログインしているユーザ情報が入っていない場合、index.jspに遷移する
		HttpSession session = request.getSession();

		if (session == null || session.getAttribute("user") == null) {
			// トップページ(チャンネル一覧)に遷移する
			request.getRequestDispatcher("index.jsp").forward(request, response);
			return;
		}

		// ログインユーザ情報を取得する
		UserDto user = (UserDto) session.getAttribute("user");

		// フォームのデータを取得する
		request.setCharacterEncoding("UTF-8");
		KnowledgeDto knowledgeDto = new KnowledgeDto();

		knowledgeDto.setKnowledge(request.getParameter("knowledge"));
		knowledgeDto.setChannelId(Integer.parseInt(request.getParameter("channelId")));
		knowledgeDto.setUserId(user.getUserId());

		// 入力チェック
		List<String> errorMessageList = FieldValidator.knowledgeValidation(knowledgeDto);
		if (errorMessageList.size() != 0) {
			// チャンネル編集画面に遷移する
			session.setAttribute("errorMessageList", errorMessageList);

			response.sendRedirect("add-knowledge");
			return;
		}

		// コネクションを取得する
		try (Connection conn = DataSourceManager.getConnection()) {
			// ナレッジを追加する
			KnowledgeDao knowledgeDao = new KnowledgeDao(conn);
			knowledgeDao.insert(knowledgeDto);

			// チャンネル名をリクエストスコープに保持する
			session.setAttribute("addMessage", "追加しました");

			// チャンネル一覧画面に遷移する
			response.sendRedirect("list-knowledge?channelId=" + knowledgeDto.getChannelId());
		} catch (SQLException | NamingException e) {

			// 例外メッセージを出力表示
			e.printStackTrace();

			// システムエラー画面に遷移する
			response.sendRedirect("system-error.jsp");
		}
	}
}
