package jp.kronos.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.kronos.DataSourceManager;
import jp.kronos.dao.UserDao;
import jp.kronos.dto.UserDto;


@WebServlet(urlPatterns={"/login"}, initParams={@WebInitParam(name="password", value="knowledge123")})
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    // セッションに保存したエラーメッセージをリクエストに移し替える
	    HttpSession session = request.getSession();
	    request.setAttribute("errorMessage", session.getAttribute("errorMessage"));
	    session.removeAttribute("errorMessage");
	    
		// ログイン画面に遷移する
		request.getRequestDispatcher("/WEB-INF/login.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// フォームのデータを取得する
		String id = request.getParameter("id");
		String password = request.getParameter("password");

		// セッションを取得する
		HttpSession session = request.getSession(true);

		// ログインID、パスワードが未入力の場合
		if ("".equals(id) || "".equals(password)) {
			session.setAttribute("errorMessage", "メールアドレス、パスワードを入力してください");
			
			// ログイン画面に遷移
			response.sendRedirect("login");
			return;
		}
		
		try (Connection conn = DataSourceManager.getConnection()) {

			// ログイン処理
			UserDao loginDao = new UserDao(conn);
			UserDto userDto = loginDao.findByIdAndPassword(id, password);

			session.setAttribute("user", userDto);
			session.removeAttribute("errorMessage");

			// ログイン失敗時
			if (userDto == null) {
				session.setAttribute("errorMessage", "メールアドレスまたはパスワードが間違っています");
				// ログイン画面に遷移
				response.sendRedirect("login");
				return;
			}

			// ログイン成功時（初回ログイン時）
			if (getInitParameter("password").equals(password)) {
				session.setAttribute("isChangeRequired", true);
				response.sendRedirect("change-password");
				return;
			}
			
			// ログイン成功時はチャンネル一覧に遷移する
			response.sendRedirect("index.jsp");
			
		} catch (SQLException | NamingException e) {
		    // 例外メッセージを出力表示
			e.printStackTrace();
			
			// システムエラー画面に遷移する
			response.sendRedirect("system-error.jsp");
		}
	}
}
