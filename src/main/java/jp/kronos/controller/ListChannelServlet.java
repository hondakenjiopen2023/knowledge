package jp.kronos.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.kronos.DataSourceManager;
import jp.kronos.dao.ChannelDao;
import jp.kronos.dto.ChannelDto;
	
@WebServlet("/list-channel")
public class ListChannelServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        // コネクションを取得する
        try (Connection conn = DataSourceManager.getConnection()) {
            // セッションを取得する
            HttpSession session = request.getSession(true);
            session.removeAttribute("queries");

            // チャンネル一覧を取得する
            ChannelDao channelDao = new ChannelDao(conn);
            List<ChannelDto> channelList = channelDao.selectAll();

            // チャンネル一覧データをリクエストに保持する
            request.setAttribute("channelList", channelList);

            // メッセージをリクエストに保持する
            request.setAttribute("message", session.getAttribute("message"));
            session.removeAttribute("message");

            // チャンネル一覧画面に遷移する
            request.getRequestDispatcher("WEB-INF/list-channel.jsp").forward(request, response);

        } catch (SQLException | NamingException e) {

            // 例外メッセージを出力表示
            e.printStackTrace();

            // システムエラー画面に遷移する
            response.sendRedirect("system-error.jsp");
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }
}
